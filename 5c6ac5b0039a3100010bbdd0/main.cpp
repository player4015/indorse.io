#include <iostream>

using namespace std;

int main(){
    int X, Y;
    cout << "Please enter no of rows (X): ";
    cin >> X;
    cout << "Please enter no of cols (Y): ";
    cin >> Y;
    if(X<1 || X>7 || Y < 1 || Y > 7){
        cout << "Invalid matrix dimensions\n";
        return 1;
    }
    // Assuming integer inputs as no input specifications are provided
    int matrix[X][Y];       // Original matrix
    int T_matrix[Y][X];     // Transposed matrix

    // input matrix
    cout << "\nInput matrix values" << endl;
    for(int i=0; i<X; ++i){
        for(int j=0; j<Y; ++j){
            cin >> matrix[i][j];
            T_matrix[j][i] = matrix[i][j];
        }
    }

    // Display original matrix
    cout << "\nInput matrix:" << endl;
    for(int i=0; i<X; ++i){
        for(int j=0; j<Y; ++j){
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    // Display transposed matrix
    cout << "\nTransposed matrix:" << endl;
    for(int i=0; i<Y; ++i){
        for(int j=0; j<X; ++j){
            cout << T_matrix[i][j] << " ";
        }
        cout << endl;
    }

    // To pause and display output
    cin.get();
}